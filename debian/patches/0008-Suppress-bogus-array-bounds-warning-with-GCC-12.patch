From: Matheus Castanho <msc@linux.ibm.com>
Date: Tue, 13 Jul 2021 14:02:22 -0300
Subject: Suppress bogus array-bounds warning with GCC 12

Building with GCC 12 is causing many errors like:

sphde/src/sassim.cpp:711:16: error: array subscript 0 is outside array bounds of
'SASBlockHeader [0]' [-Werror=array-bounds]
  711 |   anchorBlock->special = anchorBlock + 1;
      |   ~~~~~~~~~~~~~^~~~~~~

All related to anchorBlock and anchor in src/sassim.cpp:initRegion(). These look
like false-positives because anchorBlock is initialized to a fixed memory
address defined by memLow, and anchor is derived from that as well.

A similar issue with constant addresses has been reported as GCC bug 101379, in
which GCC developers advise to suppress this warning for now.

Tested with GCC 12.0.0 (20210713) and 10.2.1.

Signed-off-by: Matheus Castanho <msc@linux.ibm.com>
---
 src/sassim.cpp | 17 +++++++++++++++++
 1 file changed, 17 insertions(+)

diff --git a/src/sassim.cpp b/src/sassim.cpp
index 8f17708..d09e8f7 100644
--- a/src/sassim.cpp
+++ b/src/sassim.cpp
@@ -35,6 +35,10 @@
 #include <errno.h>
 #include <pthread.h>
 
+#if __GNUC__
+#include <features.h>
+#endif
+
 #include "sasio.h"
 #include "freenode.h"
 #include "sasanchr.h"
@@ -484,6 +488,15 @@ destroySASSem (SASAnchor_t * anchor)
 #endif
 }
 
+/* Starting with GCC 12, the compiler throws an array-bounds warning because a
+ * constant address is used to set anchorBlock, and it can't distinguish it from
+ * a NULL pointer (see GCC bug 101379). As of now, the best we can do is simply
+ * ignore it. */
+#if __GNUC__ && __GNUC_PREREQ(12,0)
+#pragma GCC diagnostic push
+#pragma GCC diagnostic ignored "-Warray-bounds"
+#endif
+
 static void
 initRegion ()
 {
@@ -565,6 +578,10 @@ initRegion ()
   initSASSem (anchor);
 }
 
+#if __GNUC__ && __GNUC_PREREQ(12,0)
+#pragma GCC diagnostic pop
+#endif
+
 int
 SASAttachSegByName (void *baseAddr, unsigned long size,
 		    int segIndex, char *name, int flags)
