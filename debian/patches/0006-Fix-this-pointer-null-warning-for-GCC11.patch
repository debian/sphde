From: Steven Munroe <munroesj52@gmail.com>
Date: Wed, 22 Jul 2020 10:52:51 -0500
Subject: Fix "'this' pointer null" warning for GCC11.

The initRegion function of sassim.cpp initializes a number of
uLongTreeNode lists. Normally uLongTreeNode lists can be empty
(root pointer NULL) at any time. This is handled internally
(correctly) even in the case of NULL "this" (insert does not depend
on "this" and uses the explicite "**root" parameter.

For SPHDE initRegion is the only place where the compiler can use
static analisis to detect "'this' pointer null". This is persistent
storage so normal operator new and constructor rules do not apply.
So we expose the internals of uLongTreeNode to sassim and provide a
static inline function to: SAS allocate and initialize a
uLongTreeNode with key/value. and store that point in the root.

	* src/sassim.cpp (initULTreeList): New function.
	(initRegion): replace insertNode call with initULTreeList,
	for lists uncommitted and region.

Signed-off-by: Steven Munroe <munroesj52@gmail.com>
---
 src/sassim.cpp | 16 +++++++++++++---
 1 file changed, 13 insertions(+), 3 deletions(-)

diff --git a/src/sassim.cpp b/src/sassim.cpp
index a652f63..d528d2c 100644
--- a/src/sassim.cpp
+++ b/src/sassim.cpp
@@ -484,6 +484,16 @@ destroySASSem (SASAnchor_t * anchor)
 #endif
 }
 
+static inline void
+initULTreeList (uLongTreeNode **root,
+		search_t keys, info_t info)
+{
+  uLongTreeNode *n = (uLongTreeNode *)SASNearAlloc(root, sizeof(uLongTreeNode));
+  n->init(keys, info);
+
+  *root = n;
+}
+
 static void
 initRegion ()
 {
@@ -517,9 +527,8 @@ initRegion ()
   sas_printf ("initRegion uncommitted %lx\n", SegmentSize);
 #endif
   nn = &(anchor->uncommitted);
-
   keys = nodeToLong (0, SizeToLog2 (SegmentSize));
-  anchor->uncommitted->insertNode (nn, keys, (unsigned long) anchorBlock);
+  initULTreeList (nn, keys, (unsigned long) anchorBlock);
 
 #ifdef __SASDebugPrint__
   sas_printf ("initRegion used %lx\n", block__Size1M);
@@ -534,7 +543,8 @@ initRegion ()
 #endif
   nn = &(anchor->region);
   keys = nodeToLong (0, SizeToLog2 (RegionSize));
-  anchor->region->insertNode (nn, keys, (unsigned long) anchorBlock);
+  initULTreeList (nn, keys, (unsigned long) anchorBlock);
+
 #ifdef __SASDebugPrint__
   sas_printf ("initRegion allocate %lx\n", SegmentSize);
 #endif
