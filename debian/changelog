sphde (1.4.0-5) unstable; urgency=medium

  * Update symbols for armhf, armel, i386

 -- Frédéric Bonnard <frediz@debian.org>  Wed, 21 Sep 2022 16:12:43 +0200

sphde (1.4.0-4) unstable; urgency=medium

  * Update changelog for 1.4.0-4 release
  * Update symbols file
  * Remove tabs from d/copyright
  * Update d/watch to format version 4
  * Upgrade d/control fields
  * Update d/copyright dates
  * List not installed explicitly in d/not-installed
  * Bump Standards-Version
  * Remove patch 0008-Allow-the-Used-block-List-tree-to-combine-adjacent-b.patch
    thanks Steve Langasek! (Closes: #1014496)

 -- Frédéric Bonnard <frediz@debian.org>  Tue, 20 Sep 2022 17:37:11 +0200

sphde (1.4.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.2.1, no changes needed.

  [ Frédéric Bonnard ]
  * Add upstream patches to fix gcc 11 compilation errors (Closes: #984345)
    and test builds with fortify.

 -- Frédéric Bonnard <frediz@debian.org>  Thu, 18 Nov 2021 17:15:46 +0100

sphde (1.4.0-2) unstable; urgency=medium

  * Workaround gbp prefixing From
  * Fix FTBFS on s390x (Closes: #906798)

 -- Frédéric Bonnard <frediz@debian.org>  Tue, 21 Aug 2018 17:36:26 +0200

sphde (1.4.0-1) unstable; urgency=medium

  * New upstream release
  * Define upstream version format of tag for gbp
  * Update maintainer details
  * Update Vcs infos
  * Upgrade debhelper compatibility level
  * Update symbol file with SPHMPMCQ symbols
  * Force main package name
  * Comply with wrap-and-sort ordering
  * Fix compilation error with gcc8 (Closes: #897869)
  * Adjust sasconf for mips64el (Closes: #844613)
  * Add mips32 support for mips and mipsel (Closes: #848233)
  * Use https for Format field of d/copyright
  * Handle Multi-Arch field
  * Bump Standards-Version
  * Remove -pie on mips and mipsel
  * Fix FTBFS mips64el with gcc8
  * Make all tests run sequentially
  * Rename the ignore patch

 -- Frédéric Bonnard <frediz@debian.org>  Mon, 20 Aug 2018 17:23:28 +0200

sphde (1.3.0-1) unstable; urgency=medium

  * New upstream release
  * Skipped some tests because of timing issues on powerpc.
  * Removed -pie build on amd64 which make gcc fail with -static
  * Added new symbols and changed one to optional.
  * Cleaned d/patches.

 -- Frédéric Bonnard <frediz@linux.vnet.ibm.com>  Wed, 31 Aug 2016 08:40:30 -0400

sphde (1.1.0-2) unstable; urgency=medium

  * Improved symbol file for better compatibility with Ubuntu and -O3
    optimisation which removes a symbols on ppc64el. Marking it optional.
  * Bumped Standards-Version to 3.9.8

 -- Frédéric Bonnard <frediz@linux.vnet.ibm.com>  Fri, 17 Jun 2016 15:08:28 +0200

sphde (1.1.0-1) unstable; urgency=low

  * Initial release (Closes: #787491)

 -- Frédéric Bonnard <frediz@linux.vnet.ibm.com>  Thu, 11 Feb 2016 14:36:40 +0100
